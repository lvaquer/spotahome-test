import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import ajax from './src/ajax'
import RoomList from './src/components/RoomList'
import RoomDetail from './src/components/RoomDetail';
import FiltersBar from './src/components/FiltersBar'


export default class App extends React.Component {

  state = {
    rooms: [],
    roomsFromSearch: [],
    currentRoomId: null,
    activeSearchTerm: '',
    noData: false,
    initialSortAsc: true
  }

  ///////////////
  // Functions //
  ///////////////
  
  //External call before mount Component
  async componentDidMount() {
    const rooms = await ajax.fetchInitialRooms();
    //console.log(rooms);

    //By default the list will be sort by pricePerMonth Asc
    rooms.sort(function (a, b) { return a.pricePerMonth - b.pricePerMonth });
    
    this.setState({ rooms });

  }

  //Set room to show RoomDetail Component
  setCurrentRoom = (roomId) => {
    this.setState({
      currentRoomId: roomId
    });
  }
  //Unset room to show RoomList Component
  unsetCurrentRoom = () => {
    this.setState({
      currentRoomId: null
    });
  }

  //SearBar Filter
  searchRooms = (searchTerm) => {
    console.log(searchTerm);

    if (searchTerm && this.searchRooms !== '') {

      var updatedRooms = this.state.rooms;
      updatedRooms = updatedRooms.filter((room) => {
        return room.title.toLowerCase().match(searchTerm.toLowerCase())
      });
      if (updatedRooms.length > 0) {
        this.setState({
          roomsFromSearch: updatedRooms,
          activeSearchTerm: searchTerm,
          noData: false
        })
      }
      else {
        this.setState({
          roomsFromSearch: [],
          activeSearchTerm: '',
          noData: true
        })
      }
    }
    else {
      this.setState({
        rooms: this.state.rooms,
        roomsFromSearch: [],
        noData: false
      });
    }
  }
  
  //Sort Price Filter
  changeSort = () => {
    
    var roomsSorted = [];
    var roomsToSort = [];

    //If there are rooms filtered by SearchBar or not
    (this.state.roomsFromSearch.length > 0) ? roomsToSort = this.state.roomsFromSearch : roomsToSort = this.state.rooms;

    //If is sort Asc/Desc
    (!this.state.initialSortAsc)
    ?
    roomsSorted = roomsToSort.sort(function (a, b) { return a.pricePerMonth - b.pricePerMonth })
    :
    roomsSorted = roomsToSort.sort(function (a, b) { return b.pricePerMonth - a.pricePerMonth })
  
    this.setState({
      rooms: roomsSorted,
      initialSortAsc: !this.state.initialSortAsc
    })
  }

  //Set RoomDetail Selected
  currentRoom = () => {
    return this.state.rooms.find((room) => room.id === this.state.currentRoomId);
  }

  ///////////////
  //// RENDER ///
  ///////////////
  render() {
    // If there is a room detail selected (render RoomDetail Component)
    if (this.state.currentRoomId) {
      return (
        <View style={styles.main}>
          <RoomDetail
            initialRoomData={this.currentRoom()}
            onBack={this.unsetCurrentRoom}
          />
        </View>
      );
    }
    // Else-If there are rooms (render RoomList Component + FiltersBar Components)
    if (this.state.rooms.length > 0) {
      var roomsToDisplay =
        this.state.roomsFromSearch.length > 0
          ? this.state.roomsFromSearch
          : this.state.rooms;
      return (
        <View style={styles.main}>
          <FiltersBar
            searchRooms={this.searchRooms}
            initialSearchTerm={this.state.activeSearchTerm}
            initialSortAsc={this.state.initialSortAsc}
            changeSort={this.changeSort}
          />
          {
            (this.state.noData)
              ? <Text style={styles.noMatches}>No Matches from Search</Text>
              : <RoomList key={this.state.initialSortAsc} rooms={roomsToDisplay} onItemPress={this.setCurrentRoom} />
          }
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {
          this.state.rooms.length > 0
            ? <RoomList rooms={this.state.rooms} onItemPress={this.setCurrentRoom} />
            : <Text style={styles.initText}>SpotaHome Test</Text>
        }
      </View>
    );
  }
}

  ///////////////
  //// STYLE ////
  ///////////////
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  main: {
    marginTop: 25
  },
  noMatches: {
    fontSize: 20,
    justifyContent: 'center',
    margin: 5,
    textAlign: 'center'
  },
  initText:{
    fontSize: 20,
    fontWeight: 'bold',
  }
});
