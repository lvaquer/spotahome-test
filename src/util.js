// UTILS METHODS


// Return String with First Uppercase Char 
export const changeFirstToUppercase = (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

export const getTypeRoom = (typeRoom) => {
    switch(typeRoom){
        case 'room_shared': return 'Habitación en piso compartido'
        default: return 'Habitación'
    }
}