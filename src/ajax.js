//EXTERNAL CALLS

const apiHost = 'https://www.spotahome.com/api/public'

export default {

    async fetchInitialRooms() {
        try {
            const response = await fetch(apiHost + '/listings/similars/122836');
            const responseJson = await response.json();
            // console.log(responseJson);
            
            return responseJson.data.homecards;
          } catch (error) {
            alert("It seems there is a PROBLEM with the CONNECTION")
            console.error(error);
          }
    }
}