import React from 'react';
import PropTypes from 'prop-types'

import { StyleSheet, Text, Image, View, TouchableOpacity } from 'react-native';
import { Icon, Rating } from 'react-native-elements';

import { changeFirstToUppercase } from '../util'


class RoomDetail extends React.Component {
   
    static propTypes = {
        initialRoomData: PropTypes.object.isRequired,
        onBack: PropTypes.func.isRequired
    }
    state = {
        room: this.props.initialRoomData,
    }

    ///////////////
    //// RENDER ///
    ///////////////
    render() {

        const { room } = this.state; //Object room
        //console.log(room);

        return (

            <View style={styles.room}>
                {/* IMAGE AND TITLE */}
                <View>
                    <TouchableOpacity style={styles.backLink} onPress={this.props.onBack}>
                        <Text style={styles.back}>&#x3c; Back</Text>
                    </TouchableOpacity>

                    <Image source={{ uri: room.mainPhotoUrl }}
                        style={styles.image}
                    />
                    <View style={styles.info}>
                        <Text style={styles.title}>{room.title}</Text>
                    </View>
                </View>
                {/* OTHER DETAILS  */}
                <View style={styles.details}>
                    {/* LEFT SIDE - DETAILS */}
                    <View style={styles.detailsLeft}>
                        {/* PRICE */}
                        <View style={styles.boxSection}>
                            <Text style={styles.titleDetailSection}>Price</Text>
                            <Text style={styles.price}>
                                {room.pricePerMonth}{room.currencySymbol}
                                <Text style={styles.pricePerMonth}>/month</Text>
                            </Text>

                        </View>
                        {/* LOCATION */}
                        <View style={styles.boxSection}>
                            <Text style={styles.titleDetailSection}>Location</Text>
                            <View style={styles.infoBoxSection}>
                                <Icon
                                    name='location-arrow'
                                    type='font-awesome'
                                    size={15}
                                    iconStyle={styles.smallIcons}
                                    color='#32b496'
                                />
                                <Text>{changeFirstToUppercase(room.city)}</Text>
                            </View>
                        </View>
                        {/* RUNNER */}
                        <View style={styles.boxSection}>
                            <Text style={styles.titleDetailSection}>Runner</Text>
                            <View style={styles.infoBoxSection}>
                                <Icon
                                    name='directions-run'
                                    type='material-icons'
                                    size={15}
                                    iconStyle={styles.smallIcons}
                                    color='#32b496'
                                />
                                {
                                    (room.runnerName)
                                        ?
                                        <Text>{room.runnerName}</Text>
                                        :
                                        <Text>No runner assigned</Text>
                                }
                            </View>
                        </View>
                    </View>
                    {/* RIGHT SIDE - DETAILS */}
                    <View style={styles.detailsRight}>
                        {/* RATING */}
                        <View style={styles.boxSection}>
                            <Text style={styles.titleDetailSection}>Rating</Text>
                            <Rating
                                imageSize={20}
                                readonly
                                startingValue={room.reviews.ratingAverage}
                            //  style={{ styles.rating }}
                            />
                        </View>
                        {/* FAVORITE */}
                        <View style={styles.boxSection}>
                            <Text style={styles.titleDetailSection}>Favorite</Text>
                            <View style={styles.flexRowDirection}>
                                <Icon name='favorite-border'
                                    color='red'
                                    size={30}
                                    iconStyle={{ marginRight: 5 }} />
                                <Text style={{ alignSelf: 'center' }}>{(room.favorite) ? " Checked" : "Unchecked"}</Text>
                            </View>
                        </View>
                        {/* INSTANT BOOKING */}
                        {
                            (room.instantBooking.isEnabled)
                                ?
                                <View style={styles.boxSection}>
                                    <Text style={styles.titleDetailSection}>Booking</Text>
                                    <View style={styles.flexRowDirection}>
                                        <Icon name='bolt'
                                            reverse
                                            type='font-awesome'
                                            color='#ffcc00'
                                            size={12}
                                        />
                                        <Text style={{ alignSelf: 'center' }}>Instant</Text>
                                    </View>
                                </View>
                                :
                                null   
                        }

                    </View>
                </View>
            </View>
        );
    }
}

  ///////////////
  //// STYLE ///
  ///////////////
const styles = StyleSheet.create({
    flexRowDirection: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        marginLeft: 10
    },
    room: {
        // marginHorizontal: 12,
        // marginTop: 30,
    },
    backLink: {
        position: 'absolute',
        left: 10,
        top: 15,
        zIndex: 5,
    },
    back: {
        marginBottom: 7,
        marginLeft: 8,
        color: '#32b496',
        fontWeight: 'bold',
    },
    image: {
        width: '100%',
        height: 250
    },
    info: {
        padding: 10,
        backgroundColor: '#fff',
        borderColor: '#32b496',
        borderWidth: 1,
        borderTopWidth: 0,
    },
    title: {
        fontSize: 16,
        marginRight: 5,
    },
    details: {
        flexDirection: 'row',
        height: 300
    },
    detailsLeft: {
        flex: 1,
    },
    detailsRight: {
        flex: 1,
        borderLeftWidth: 1,
        borderLeftColor: '#32b496',
    },
    titleDetailSection: {
        color: '#a6a6a6',
        paddingLeft: 5,
        paddingBottom: 1,
        paddingTop: 5,
    },
    boxSection: {
        paddingTop: 5,
    },
    price: {
        paddingLeft: 20,
        marginRight: 5,
        fontSize: 19,
        fontWeight: 'bold',
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    pricePerMonth: {
        fontSize: 10
    },
    infoBoxSection: {
        flexDirection: 'row',
    },
    smallIcons: {
        marginLeft: 20,
        marginRight: 5,
    }

});

export default RoomDetail;