import React from 'react';
import PropTypes from 'prop-types'

import { StyleSheet, FlatList, View } from 'react-native';

import RoomItem from './RoomItem'


class RoomList extends React.Component {

    static propTypes = {
        rooms: PropTypes.array.isRequired,
        onItemPress: PropTypes.func.isRequired
    }

    //////////////
    // RENDER ///
    /////////////
    render() {
        return (
            <View style={styles.list}>
                <FlatList
                    data={this.props.rooms}
                    renderItem={({ item }) =>
                        <RoomItem 
                            key={item.id} 
                            room={item}
                            onPress={this.props.onItemPress}
                        />}
                />
            </View>
        );
    }
}

  ///////////////
  //// STYLE ////
  ///////////////
const styles = StyleSheet.create({

    list: {
        backgroundColor: '#eee',
        width: '100%',
    },
})

export default RoomList;