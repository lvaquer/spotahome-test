import React from 'react';

import PropTypes from 'prop-types';

import { View, TextInput, StyleSheet, Text,TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

// SearchBar and Sort Filters
class FiltersBar extends React.Component {

    static propTypes = {
        searchRooms: PropTypes.func.isRequired,
        initialSearchTerm: PropTypes.string.isRequired,
        initialSortAsc: PropTypes.bool.isRequired,
        changeSort: PropTypes.func.isRequired
    };
    state = {
        searchTerm: '',
        sortAsc: true,
        current: 0 //varible to rerender searchBar as a key prop
    };
    ///////////////
    // Functions //
    ///////////////
    searchRooms = (searchTerm) => {
        this.props.searchRooms(searchTerm)
    }
    handleChange = (searchTerm) => {

        this.setState({
            searchTerm
        });
        if (searchTerm != '')
            this.searchRooms(searchTerm)
    }
    changeSortList = () => {
        this.props.changeSort()
        this.setState({
            sortAsc: !this.state.sortAsc
        });
    }

    cleanSearchBar = () => {
        console.log("clean Search");
        this.searchRooms('')
        this.setState({
            searchTerm: '',
            current: this.state.current+1
        });    
    }

    ///////////////
    /// RENDER ///
    ///////////////
    render() {
        return (
            <View style={styles.content}>
                {/* SEARCH BAR */}
                <View style={styles.searchBar}>
                    <TextInput
                        style={styles.input}
                        key={this.state.current}
                        value={this.searchTerm}
                        placeholder="Find by title..."
                        autoCorrect={false}
                        onChangeText={this.handleChange}
                        underlineColorAndroid='transparent'
                    >
                    </TextInput>
                    <TouchableOpacity style={styles.clearInput} onPress={this.cleanSearchBar}>
                        <Text style={styles.textClearInput}>Cancel</Text>
                    </TouchableOpacity>
                </View>
                {/* SORT BUTTON */}
                <View style={styles.sortLabel}>
                    <Text style={styles.sortText}
                        onPress={this.changeSortList}
                    >
                        Sort by Price
                    </Text>
                    {
                        (this.state.sortAsc)
                            ?
                            <Icon
                                iconStyle={styles.sortIcon}
                                name='sort-numeric-desc'
                                type='font-awesome'
                                color='#32b496'
                                size={14} />
                            :
                            <Icon
                                iconStyle={styles.sortIcon}
                                name='sort-numeric-asc'
                                type='font-awesome'
                                color='#32b496'
                                size={14} />
                    }
                </View>
            </View>
        );
    }
}

///////////////
//// STYLE ////
///////////////

const styles = StyleSheet.create({

    content: {
        width: '100%',
    },
    searchBar:{
        display:'flex',
        flexDirection: 'row'
    },
    input: {
        flex:4,
        margin: 14,
        height: 26,
        fontSize: 20,
        color: '#000',
        borderBottomWidth: 1,
        borderBottomColor: '#32b496',
    },
    clearInput:{
        flex:1,
        alignSelf: 'center',
        alignItems: 'center',
        borderWidth:1,
        borderColor: '#32b496',
        borderRadius: 5,
        marginRight: 10,
        paddingVertical: 5,
        paddingHorizontal: 7,

    },
    textClearInput:{
        justifyContent:'center'
    },
    sortLabel: {
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'center',
        marginBottom: 4,
    },
    sortText: {
        fontSize: 16,
        paddingVertical: 5,
        color: '#32b496',
        height: 30,
        marginRight: 5,
        textDecorationLine: 'underline'
    },
    sortIcon: {
        fontSize: 16,
        paddingVertical: 5,
        height: 30,
    },

});

export default FiltersBar;