import React from 'react';
import PropTypes from 'prop-types'

import { StyleSheet, Text, Image, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { getTypeRoom } from '../util'


class RoomItem extends React.Component {
    
    static propTypes = {
        room: PropTypes.object.isRequired,
        onPress: PropTypes.func.isRequired,
    }

    ///////////////
    // Functions //
    ///////////////
    handlePress = () => {
        this.props.onPress(this.props.room.id)
    }

    ///////////////
    //// RENDER //
    //////////////
    render() {
        const { room } = this.props;
        return (
            <View>
                <TouchableOpacity
                    style={styles.room}
                    onPress={this.handlePress}
                >
                    <Image source={{ uri: room.photoUrls.homecard }}
                        style={styles.image}
                    />
                    {/* FAVORITE ICON */}
                    {
                        (room.favorite)
                            ? 
                            <View style={styles.favIcon}>
                                <Icon name='favorite'
                                    color='red'
                                    size={30} />
                            </View>
                            : 
                            <View style={styles.favIcon}>
                                <Icon name='favorite-border'
                                    color='red'
                                    size={30} />
                            </View>
                    }
                    {/* INSTANT BOOKING ICON */}
                    {
                        (room.instantBooking.isEnabled)
                            ? 
                            <View style={styles.instantIcon}>
                                <Icon name='bolt'
                                    reverse
                                    type='font-awesome'
                                    color='#ffcc00'
                                    size={15} />
                            </View>
                            : null
                    }
                    {/* DETAILS (TITLE - PRICE) */}
                    <View style={styles.info}>
                        <View style={styles.footer}>
                            <Text style={styles.title}>{room.title}</Text>
                            <View style={styles.priceBox}>
                                <Text style={styles.price}>{room.pricePerMonth}{room.currencySymbol}</Text>
                                <Text style={styles.perMonth}>/month</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

  ///////////////
  //// STYLE ///
  ///////////////

const styles = StyleSheet.create({
    room: {
        marginHorizontal: 12,
        marginTop: 12,
    },
    image: {
        width: '100%',
        height: 250
    },
    favIcon: {
        position: 'absolute',
        right: 10,
        top: 10,
    },
    instantIcon: {
        position: 'absolute',
        left: 10,
        top: 10,
    },
    info: {
        padding: 10,
        backgroundColor: '#fff',
        borderColor: '#bbb',
        borderWidth: 1,
        borderTopWidth: 0,
    },
    footer: {
        flexDirection: 'row',
    },
    title: {
        flex: 4,
        fontSize: 16,
        marginRight: 5,
    },
    priceBox: {
        flex: 1,
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#32b496',
        paddingVertical: 5,
    },
    price: {
        fontSize: 16,
        textAlign: 'center'
    },
    perMonth: {
        textAlign: 'center',
        fontSize: 10
    }
});

export default RoomItem;